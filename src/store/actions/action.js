import { USER_LOGIN_SUCCESS } from "./type";
import { loginUserApi } from './api';

export const loginUser = (loginData, redirect, errorNotification) => async dispatch => {

    try {
        // API call
        const data = await loginUserApi(loginData);
        if (data) {
            dispatch({ type: USER_LOGIN_SUCCESS, data: data })
            if (redirect) redirect()
        }
        if (data.errorCode) {
            if (errorNotification) errorNotification(data.errorCode)
        }
       
    } catch (e) {
        console.error(e);
       
        if (errorNotification) errorNotification('generic_error')
    }
};

export const logoutUser = () => async dispatch => {
    // API call
    dispatch({ type: USER_LOGIN_SUCCESS, data: {} })
};