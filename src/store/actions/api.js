import axios from 'axios';

export const loginUserApi = async (data) => {
    try {
        const response = await axios.post('https://books.ioasys.com.br/api/v1/auth/sign-in', data);
        return response
    } catch (e) {
        console.error(e);
    }
};