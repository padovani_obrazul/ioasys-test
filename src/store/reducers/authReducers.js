import { USER_LOGIN_SUCCESS } from "../actions/type";

const init = {
    user: {}
};

const authReducer = (state = init, action) => {
    switch (action.type) {
        case USER_LOGIN_SUCCESS: {
            return { ...state, user: action.data };
        }
        default:
            return state;
    }
};

export const selectedUser = (state = init) => state.auth;

export default authReducer