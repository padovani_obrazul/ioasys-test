import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import rootReducer from './reducers/rootReducers';
import storage from 'redux-persist/lib/storage'

const middleware = [thunk];

const persistConfig = {
    key: 'root',
    storage,
    blacklist: [], // redurcers that should not be persisted,
    whitelist: ['auth'] // redurcers that will be persisted
};

const persistedReducer = persistReducer(persistConfig, rootReducer)

let store = createStore(persistedReducer, compose(
    applyMiddleware(...middleware)
))
let persistor = persistStore(store)
export { store, persistor }