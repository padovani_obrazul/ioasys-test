import React from 'react';

import styles from './styles.module.scss';

export const Carousel = ({ books, renderBookModal }) => {
    return (
        books.map((book, index) => (
            <div onClick={() => renderBookModal(book.id)} key={index} className={styles.bookSummary}>
                <div className={styles.bookInfo}>
                    <div className={styles.image}>
                        <img src={book.imageUrl} alt="book carousel" />
                    </div>
                    <div className={styles.otherInfo}>
                        <span>{book.title}</span>
                        <span>{book.title}</span>
                        <div className={styles.otherInfo__footerItems}>
                            <div className={styles.otherInfo__footer}>{book.pageCount} páginas</div>
                            <div className={styles.otherInfo__footer}>Editora {book.publisher}</div>
                            <div className={styles.otherInfo__footer}>Publicado em {book.pageCount}</div>
                        </div>
                    </div>
                </div>
            </div>
        ))
    )
}