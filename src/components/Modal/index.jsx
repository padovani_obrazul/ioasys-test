import React from 'react';

import styles from './styles.module.scss';

export const Modal = ({ bookModal, setOpenModal }) => {
    return (
        <div className={styles.bookModalWrapper}>
            <button onClick={() => setOpenModal(false)} className={styles.bookModalWrapper__closeModal}></button>
            <div className={styles.bookModalWrapper__modal}>
                <div className={styles.bookInfo}>
                    <div className={styles.bookInfo__image}>
                        <img src={bookModal.imageUrl} alt="" />
                    </div>
                    <div className={styles.bookInfo__details}>
                        <h1>{bookModal.title}</h1>
                        <small>{bookModal.authors.map((author, index) => (
                            <span key={index}>{author}, </span>
                        ))}
                        </small>
                        <h2>INFORMAÇÕES</h2>
                        <div className={styles.summary}>
                            <div className={styles.summary__index}>
                                <div>Páginas</div>
                                <div>Editora</div>
                                <div>Publicação</div>
                                <div>Idioma</div>
                                <div>Título Original</div>
                                <div>ISBN-10</div>
                                <div>ISBN-13</div>
                            </div>
                            <div className={styles.summary__info}>
                                <div>{bookModal.pageCount}</div>
                                <div>{bookModal.publisher}</div>
                                <div>{bookModal.published}</div>
                                <div>{bookModal.language}</div>
                                <div>{bookModal.title}</div>
                                <div>{bookModal.isbn10}</div>
                                <div>{bookModal.isbn13}</div>
                            </div>
                        </div>
                        <h2>RESENHA DA EDITORA</h2>
                        <div className={styles.bookInfo__description}>
                            <p>{bookModal.description}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}