import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import axios from 'axios'
import { CircularProgress } from '@material-ui/core';
import { useRouter } from 'next/router';

import { useSelector } from 'react-redux';
import { selectedUser } from '../store/reducers/authReducers';
import { logoutUser } from '../store/actions/action';
import { Modal } from '../components/Modal';
import { Carousel } from '../components/Carousel';

import styles from './home.module.scss';

export default function Home() {
    const userAutenticated = useSelector(selectedUser);
    const dispatch = useDispatch();
    const [page, setPage] = useState(1)
    const [books, setBooks] = useState([])
    const [booksInfo, setBooksInfo] = useState([])
    const [openModal, setOpenModal] = useState(false)
    const [bookModal, setBookModal] = useState([])
    const [loading, setLoading] = useState(false)
    const router = useRouter();

    const renderBookModal = async (bookId) => {
        setLoading(true);
        
        const token = userAutenticated.user.headers.authorization;
        const refreshToken = userAutenticated.user.headers["refresh-token"]
        
        const response = await axios({
            method: 'get',
            url: `https://books.ioasys.com.br/api/v1/books/${bookId}`,
            headers: {
                'Authorization': `Bearer ${token}`
            },
            body: {
                "refreshToken": `Bearer ${refreshToken}`
            }
        });
        
        setBookModal(response.data)
        setOpenModal(true);
        setLoading(false);
    }

    const getBooksData = async () => {
        const token = userAutenticated.user.headers.authorization;
        const refreshToken = userAutenticated.user.headers["refresh-token"];

        const response = await axios({
            method: 'get',
            url: `https://books.ioasys.com.br/api/v1/books?page=${page}&amount=12`,
            headers: {
                'Authorization': `Bearer ${token}`
            },
            body: {
                "refreshToken": `Bearer ${refreshToken}`
            }
        });

        return response;
    }

    const changePage = async (page) => {
        setLoading(true);
        const response = await getBooksData();
        setBooks(response.data.data)
        setLoading(false);
    }

    const prevPage = () => {
        if (page < 1) return 
        setPage(page - 1)

        changePage(page)
    }

    const nextPage = () => {
        setPage(page + 1)

        changePage(page)
    }
   
    useEffect(() => {
        (async () => {
            if (userAutenticated?.user?.status === 200) {
                const response = await getBooksData();

                setBooks(response.data.data);
                setBooksInfo(response.data);
            } else {
                router.push('/')
            }
        })()
    }, [userAutenticated])

    const logoutHandler = () => {
        setLoading(true);
        dispatch(logoutUser())
        router.push('/')
        setLoading(false);
    }

    return (
        <div className={styles.homeWrapper}>
            
            {loading &&
                <div className={styles.loading}>
                    <CircularProgress color="secondary"/>
                </div>
            }

            {openModal && bookModal &&
                <Modal 
                    bookModal={bookModal}
                    setOpenModal={setOpenModal}
                />
            }
    
            <Grid item xs={12} className={styles.headerWrapper}>
                <Grid item xs={6} className={styles.homeLogo}>
                    <img className={styles.Logo} src="/images/LogoBlack.png" style={{ height: '36px' }} alt="shape" />
                    <span>Books</span>
                </Grid>
                <Grid item xs={6} className={styles.welcomeUser}>
                    {userAutenticated?.user?.data && 
                        <span>Bem-vindo, <strong>{userAutenticated.user.data.name}</strong>!</span>
                    }
                    <button className={styles.logOutButton} onClick={logoutHandler}></button>
                </Grid>
            </Grid>
            <Grid className={styles.clothingWapper}>
                  
                <Grid className={styles.clothingContent}>

                    <div className={styles.booksCarouselContainer}>     
                        {books.length > 0 &&
                            <Carousel 
                                books={books}
                                renderBookModal={renderBookModal}
                            />
                        }
                    </div>
                </Grid>
            </Grid>

            <Grid item xs={12} className={styles.nextPageWrapper}>
                <div className={styles.nextPrevContainer}>
                    <span>Página {page} de {parseInt(booksInfo.totalPages)}</span>
                    <button onClick={prevPage} className={styles.prevChevron}></button>
                    <button onClick={nextPage}  className={styles.nextChevron}></button>
                </div>
            </Grid>
            
            <img className={styles.bgHomeDesk} src="/images/bgHomeDesk2.png" style={{ height: '100%'}}  alt="shape" />
        </div>
        
    )
}