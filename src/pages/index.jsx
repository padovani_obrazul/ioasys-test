import React, { useState } from 'react'
import { useFormik } from 'formik';
import { useDispatch } from 'react-redux';
import { toast } from "react-toastify";
import * as yup from 'yup';
import { CircularProgress } from '@material-ui/core';
import { useRouter } from 'next/router';

//material ui
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";

import { getErrorMessage } from '../utils/errorMessage';
import { loginUser } from '../store/actions/action';

import styles from './login.module.scss';

const validationSchema = yup.object().shape({
    email: yup.string().email('Email inválido').required('Preenchimento obrigatório'),
    password: yup.string().required('Preenchimento obrigatório')
})

const useStyles = makeStyles((theme) => ({
    input: {
        color: '#ffffff',
        "& .MuiInput-underline:after": {
            borderBottomColor: "green"
          },
    },
    label: {
        color: '#ffffff'
    }
}));

export default function Login() {
    const dispatch = useDispatch();
    const classes = useStyles();
    const [loading, setLoading] = useState(false)
    const [showError, setShowError] = useState(false)
    const router = useRouter();

    const redirectOnSuccess = () => {
      router.push('/home')
      setLoading(false)
    }

    const errorNotification = (errorCode) => {
        toast.error(getErrorMessage[errorCode])
        setLoading(false)
        setShowError(true)
    }

    const logUser = (user) => {
        setLoading(true)
        
        dispatch(loginUser({ email: user.email, password: user.password }, redirectOnSuccess, errorNotification))
    }

    const { 
        values,   
        handleChange, 
        handleBlur, 
        handleSubmit
      } = useFormik({
        onSubmit: logUser,
        validationSchema,
        initialValues: {
          email: '',
          password: ''
        }
      })
    return (
        <Grid container className={styles.loginWrapper}>
            {loading &&
                <div className={styles.loadingLogin}>
                    <CircularProgress color="secondary"/>
                </div>
            }
            <Grid
                container
                className={styles.container}>
                <Grid item xs={12} md={5} className={styles.contentsContainer}>
                    <Grid className={styles.heroContent}>
                        <Grid className={styles.titleInputs}>
                            <img className={styles.Logo} src="/images/Logo@2x.png" style={{ height: '36px' }} alt="shape" />
                            <span>Books</span>
                        </Grid>
                        <Grid container>
                        <form onSubmit={handleSubmit}>
                            <Grid item xs={9} md={10}>
                                <TextField
                                    id="standard-email"
                                    label="Email"
                                    name="email"
                                    className={styles.inputStyleBasic}
                                    margin="normal"
                                    variant="filled"
                                    type="text"
                                    InputProps={{
                                        className: classes.input
                                    }}
                                    onChange={handleChange}
                                    value={values.email}
                                    onBlur={handleBlur}
                                />
                            </Grid>
                            <Grid item xs={9} md={10}>
                                <TextField
                                    id="standard-password"
                                    label="Senha"
                                    name="password"
                                    className={styles.inputStyleBasic}
                                    margin="normal"
                                    variant="filled"
                                    type="password"
                                    onChange={handleChange}
                                    value={values.password}
                                    onBlur={handleBlur}
                                    InputProps={{
                                        className: classes.input,
                                        endAdornment: <Button 
                                                        className={styles.submitButton}
                                                        type="submit"
                                                        >Entrar
                                                      </Button>
                                    }}
                                />
                                {showError &&
                                    <div onClick={() => setShowError(false)} className={styles.loginErrorMsg}>
                                        <p>Email e/ou senha incorretos</p>
                                    </div>
                                }
                            </Grid>
                        </form>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12} md={7}>
                    <Grid className={styles.hrRighThumb}>
                        
                    </Grid>
                </Grid>
            </Grid>
            <img className={styles.bgDeskLogin} src="/images/bgDeskLogin.png"  alt="shape" />
            <img className={styles.bgMobileLogin} src="/images/bgMobileLogin.png"  alt="shape" />
        </Grid>
    )
}