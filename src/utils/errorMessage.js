export const getErrorMessage = {
    'email_already_exists': "Já existe no sistema uma conta que utiliza este endereço de e-mail.",
    'invalid_email_address': "Por favor, forneça um endereço de e-mail válido",
    'incorrect_credentials': "As credenciais fornecidas não estão corretas.",
    'generic_error': "O sistema encontrou um erro inesperado. Por favor, tente novamente mais tarde.",
}